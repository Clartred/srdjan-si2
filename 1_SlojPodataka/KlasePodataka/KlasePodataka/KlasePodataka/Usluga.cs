﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KlasePodataka
{
    public class Usluga
    {
        private int id;
        private int idKorisnika;
        private string naziv;
        private DateTime rok;
        private int cena;
        private string opis;
        private DateTime pocetakIzrade;
        private DateTime krajIzrade;
        private bool placena = false;
        private VrstaUsluge vrstaUsluge;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public int IdKorisnika
        {
            get { return idKorisnika; }
            set { idKorisnika = value; }
        }

        public string Naziv
        {
            get { return naziv; }
            set { naziv = value; }
        }

        public int Cena
        {
            get { return cena; }
            set { cena = value; }
        }
        public string Opis
        {
            get { return opis; }
            set { opis = value; }
        }
        public DateTime Rok
        {
            get { return rok; }
            set { rok = value; }
        }
        public DateTime PocetakIzrade
        {
            get { return pocetakIzrade; }
            set { pocetakIzrade = value; }
        }

        public DateTime KrajIzrade
        {
            get { return krajIzrade; }
            set { krajIzrade = value; }
        }

        public bool Placena
        {
            get { return placena; }
            set { placena = value; }
        }
        public VrstaUsluge VrstaUsluge
        {
            get { return vrstaUsluge; }
            set { vrstaUsluge = value; }
        }
    }
}
