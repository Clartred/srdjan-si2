﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace KlasePodataka
{
    public class UslugaDB
    {
        private string pStringKonekcije;

        public string StringKonekcije
        {
            get
            {
                return pStringKonekcije;
            }
            set
            {
                if (this.pStringKonekcije != value)
                    this.pStringKonekcije = value;
            }
        }

        public UslugaDB(string NoviStringKonekcije)
        {
            pStringKonekcije = NoviStringKonekcije;
        }


        public DataSet DajSveUsluge()
        {

            DataSet dsPodaci = new DataSet();

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();
            SqlCommand Komanda = new SqlCommand("DajSveUsluge", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = Komanda;
            da.Fill(dsPodaci);
            Veza.Close();
            Veza.Dispose();

            return dsPodaci;
        }

        public DataSet DajSveUslugePoIdKorisnika(int idKorisnika)
        {

            DataSet dsPodaci = new DataSet();

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();
            SqlCommand Komanda = new SqlCommand("DajSveUslugePoIdKorisnika", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@IdKorisnika", SqlDbType.Int).Value = idKorisnika;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = Komanda;
            da.Fill(dsPodaci);
            Veza.Close();
            Veza.Dispose();

            return dsPodaci;
        }

        public DataSet DajUsluguPoId(int id)
        {
            DataSet dsPodaci = new DataSet();
            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();
            SqlCommand Komanda = new SqlCommand("DajUsluguPoId", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@Id", SqlDbType.NVarChar).Value = id;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = Komanda;
            da.Fill(dsPodaci);
            Veza.Close();
            Veza.Dispose();

            return dsPodaci;
        }

        public DataSet DajUslugePoVrstiUsluge(int IdVrsteUsluge)
        {
            DataSet dsPodaci = new DataSet();

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();
            SqlCommand Komanda = new SqlCommand("DajUsluguPoVrstiUsluge", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@Id", SqlDbType.NVarChar).Value = IdVrsteUsluge;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = Komanda;
            da.Fill(dsPodaci);
            Veza.Close();
            Veza.Dispose();

            return dsPodaci;
            
        }

        public bool SnimiNovuUslugu(Usluga usluga)
        {

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();

            SqlCommand Komanda = new SqlCommand("SnimiNovuUslugu", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@Id", SqlDbType.Int).Value = usluga.Id;
            Komanda.Parameters.Add("@IdKorisnika", SqlDbType.Int).Value = usluga.IdKorisnika;
            Komanda.Parameters.Add("@Naziv", SqlDbType.NVarChar).Value = usluga.Naziv;
            Komanda.Parameters.Add("@Rok ", SqlDbType.DateTime).Value = usluga.Rok;
            Komanda.Parameters.Add("@PocetakIzrade ", SqlDbType.DateTime).Value = usluga.PocetakIzrade;
            Komanda.Parameters.Add("@KrajIzrade ", SqlDbType.DateTime).Value = usluga.KrajIzrade;
            Komanda.Parameters.Add("@Cena", SqlDbType.Int).Value = usluga.Cena;
            Komanda.Parameters.Add("@Opis", SqlDbType.NVarChar).Value = usluga.Opis;
            Komanda.Parameters.Add("@Placena", SqlDbType.Bit).Value = usluga.Placena;
            Komanda.Parameters.Add("@IdVrsteUsluge", SqlDbType.Int).Value = usluga.VrstaUsluge.Id;

            int brojSlogova = Komanda.ExecuteNonQuery();
            Veza.Close();
            Veza.Dispose();


            return (brojSlogova > 0);
        }

        public bool ObrisiUslugu(int id)
        {

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();

            SqlCommand Komanda = new SqlCommand("ObrisiUslugu", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@Id", SqlDbType.Int).Value = id;
            int brojSlogova = Komanda.ExecuteNonQuery();
            Veza.Close();
            Veza.Dispose();

            return (brojSlogova > 0);
        }

        public bool IzmeniUslugu(int id, Usluga usluga)
        {

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();

            SqlCommand Komanda = new SqlCommand("IzmeniUslugu", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@Id", SqlDbType.Int).Value = usluga.Id;
            Komanda.Parameters.Add("@IdKorisnika", SqlDbType.Int).Value = usluga.IdKorisnika;
            Komanda.Parameters.Add("@Naziv", SqlDbType.NVarChar).Value = usluga.Naziv;
            Komanda.Parameters.Add("@Rok ", SqlDbType.DateTime).Value = usluga.Rok;
            Komanda.Parameters.Add("@PocetakIzrade ", SqlDbType.DateTime).Value = usluga.PocetakIzrade;
            Komanda.Parameters.Add("@KrajIzrade ", SqlDbType.DateTime).Value = usluga.KrajIzrade;
            Komanda.Parameters.Add("@Cena", SqlDbType.Int).Value = usluga.Cena;
            Komanda.Parameters.Add("@Opis", SqlDbType.NVarChar).Value = usluga.Opis;
            Komanda.Parameters.Add("@Placena", SqlDbType.Bit).Value = usluga.Placena;
            Komanda.Parameters.Add("@IdVrsteUsluge", SqlDbType.Int).Value = usluga.VrstaUsluge.Id;

            int brojSlogova = Komanda.ExecuteNonQuery();
            Veza.Close();
            Veza.Dispose();

            return (brojSlogova > 0);
        }

        public bool IzmeniUslugu(Usluga objStaraUsluga, Usluga objNovaUsluga)
        {
            int brojSlogova = 0;
            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();

            SqlCommand Komanda = new SqlCommand("IzmeniUslugu", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@StariId", SqlDbType.Int).Value = objStaraUsluga.Id;
            Komanda.Parameters.Add("@Id", SqlDbType.Int).Value = objNovaUsluga.Id;
            Komanda.Parameters.Add("@IdKorisnika", SqlDbType.Int).Value = objStaraUsluga.IdKorisnika;
            Komanda.Parameters.Add("@Naziv", SqlDbType.NVarChar).Value = objStaraUsluga.Naziv;
            Komanda.Parameters.Add("@Rok", SqlDbType.DateTime).Value = objStaraUsluga.Rok;
            Komanda.Parameters.Add("@Cena", SqlDbType.Int).Value = objStaraUsluga.Cena;
            Komanda.Parameters.Add("@PocetakIzrade", SqlDbType.DateTime).Value = objStaraUsluga.PocetakIzrade;
            Komanda.Parameters.Add("@KrajIzrade", SqlDbType.DateTime).Value = objStaraUsluga.KrajIzrade;
            Komanda.Parameters.Add("@Placena", SqlDbType.Bit).Value = objStaraUsluga.PocetakIzrade;
            Komanda.Parameters.Add("@IdVrsteUsluge", SqlDbType.Int).Value = objStaraUsluga.VrstaUsluge.Id;

            brojSlogova = Komanda.ExecuteNonQuery();
            Veza.Close();
            Veza.Dispose();

            return (brojSlogova > 0);

        }

        public bool PlatiUslugu(int id)
        {

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();

            SqlCommand Komanda = new SqlCommand("PlatiUslugu", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@Id", SqlDbType.Int).Value = id;
           
            int brojSlogova = Komanda.ExecuteNonQuery();
            Veza.Close();
            Veza.Dispose();

            return (brojSlogova > 0);
        }
    }
}
