﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KlasePodataka
{
    public class UslugaLista
    {
        private List<Usluga> listaUsluga;
        public List<Usluga> ListaUsluga
        {
            get
            {
                return listaUsluga;
            }
            set
            {
                if (this.listaUsluga != value)
                    this.listaUsluga = value;
            }
        }

        public UslugaLista()
        {
            listaUsluga = new List<Usluga>();
        }

        public void DodajElementListe(Usluga usluga)
        {
            listaUsluga.Add(usluga);
        }

        public void ObrisiElementNaPoziciji(int pozicija)
        {
            listaUsluga.RemoveAt(pozicija);
        }

        public void IzmeniElementListe(Usluga staraUsluga, Usluga novaUsluga)
        {
            int idStareUsluge = listaUsluga.IndexOf(staraUsluga);
            listaUsluga.RemoveAt(idStareUsluge);
            listaUsluga.Insert(idStareUsluge, novaUsluga);
        }
    }
}
