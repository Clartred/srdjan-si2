﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KlasePodataka
{
    public class VrstaUsluge
    {
        private int id;
        private string naziv;
        private int pocetnaCena;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public string Naziv
        {
            get { return naziv; }
            set { naziv = value; }
        }

        public int PocetnaCena
        {
            get { return pocetnaCena; }
            set { pocetnaCena = value; }
        }
    }
}
