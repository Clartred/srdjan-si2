﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace KlasePodataka
{
    public class VrstaUslugeDB
    {
        private string pStringKonekcije;

        public string StringKonekcije
        {
            get
            {
                return pStringKonekcije;
            }
            set
            {
                if (this.pStringKonekcije != value)
                    this.pStringKonekcije = value;
            }
        }

        public VrstaUslugeDB(string NoviStringKonekcije)

        {
            pStringKonekcije = NoviStringKonekcije;
        }

        public DataSet DajSveVrsteUsluga()
        {

            DataSet dsPodaci = new DataSet();

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();
            SqlCommand Komanda = new SqlCommand("DajSveVrsteUsluga", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = Komanda;
            da.Fill(dsPodaci);
            Veza.Close();
            Veza.Dispose();

            return dsPodaci;
        }

        public DataSet DajNazivVrsteUslugePoId(int idVrsteUsluga)
        {
            DataSet dsPodaci = new DataSet();

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();
            SqlCommand Komanda = new SqlCommand("DajNazivVrsteUslugePoId", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@Id", SqlDbType.Int).Value = idVrsteUsluga;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = Komanda;
            da.Fill(dsPodaci);
            Veza.Close();
            Veza.Dispose();

            return dsPodaci;
        }

        public string DajNazivVrsteUslugePremaId(int Id)
        {
            string NazivVrsteUsluge = "";
            DataSet dsPodaci = new DataSet();

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();
            SqlCommand Komanda = new SqlCommand("DajNazivVrsteUslugePoId", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@Id", SqlDbType.Int).Value = Id;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = Komanda;
            da.Fill(dsPodaci);
            Veza.Close();
            Veza.Dispose();

            NazivVrsteUsluge = dsPodaci.Tables[0].Rows[0].ItemArray[1].ToString();

            return NazivVrsteUsluge;
        }

        //Dodato naknadno
        public string DajIdUslugePoNazivu(string naziv)
        {

            DataSet dsPodaci = new DataSet();

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();
            SqlCommand Komanda = new SqlCommand("DajUsluguPoNazivu", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@Naziv", SqlDbType.NVarChar).Value = naziv;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = Komanda;
            da.Fill(dsPodaci);
            Veza.Close();
            Veza.Dispose();

            return dsPodaci.Tables[0].Rows[0].ItemArray[0].ToString();
        }

        //Dodato naknadno
        public DataSet DajVrstUslugePoNazivu(string naziv)
        {
          
            DataSet dsPodaci = new DataSet();

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();
            SqlCommand Komanda = new SqlCommand("DajUsluguPoNazivu", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@Naziv", SqlDbType.NVarChar).Value = naziv;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = Komanda;
            da.Fill(dsPodaci);
            Veza.Close();
            Veza.Dispose();
            return dsPodaci;
        }

        public bool SnimiNovuVrstuUsluge(VrstaUsluge vrstaUsluge)
        {

            int brojSlogova = 0;

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();

            SqlCommand Komanda = new SqlCommand("DodajNoviTim", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@Id", SqlDbType.Int).Value = vrstaUsluge.Id;
            Komanda.Parameters.Add("@Naziv", SqlDbType.NVarChar).Value = vrstaUsluge.Naziv;
            Komanda.Parameters.Add("@PocetnaCena", SqlDbType.NVarChar).Value = vrstaUsluge.PocetnaCena;

            brojSlogova = Komanda.ExecuteNonQuery();
            Veza.Close();
            Veza.Dispose();

            return (brojSlogova > 0);
        }

        public bool ObrisiVrstuUsluge(int id)
        {

            int brojSlogova = 0;

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();

            SqlCommand Komanda = new SqlCommand("ObrisiVrstuUsluge", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@Id", SqlDbType.Int).Value = id;
            brojSlogova = Komanda.ExecuteNonQuery();
            Veza.Close();
            Veza.Dispose();

            return (brojSlogova > 0);
        }

        //Dodato naknadno
        public bool IzmeniVrstuUsluge(VrstaUsluge objStaraVrstaUsluge, VrstaUsluge objNovaVrstaUsluge)
        {
            int brojSlogova = 0;
            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();
            SqlCommand Komanda = new SqlCommand("IzmeniVrstuUsluge", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@StariIdVrsteUsluge", SqlDbType.Int).Value = objStaraVrstaUsluge.Id;
            Komanda.Parameters.Add("@Id", SqlDbType.Int).Value = objNovaVrstaUsluge.Id;
            Komanda.Parameters.Add("@Naziv", SqlDbType.NVarChar).Value = objNovaVrstaUsluge.Naziv;
            Komanda.Parameters.Add("@PocetnaCena", SqlDbType.NVarChar).Value = objNovaVrstaUsluge.PocetnaCena;
            brojSlogova = Komanda.ExecuteNonQuery();
            Veza.Close();
            Veza.Dispose();
            return (brojSlogova > 0);
        }

        public bool IzmeniVrstuUsluge(int id, VrstaUsluge novaVrstaUsluge)
        {

            int brojSlogova = 0;

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();

            SqlCommand Komanda = new SqlCommand("IzmeniVrstuUsluge", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@Id", SqlDbType.Int).Value = novaVrstaUsluge.Id;
            Komanda.Parameters.Add("@Naziv", SqlDbType.NVarChar).Value = novaVrstaUsluge.Naziv;
            Komanda.Parameters.Add("@PocetnaCena", SqlDbType.NVarChar).Value = novaVrstaUsluge.PocetnaCena;
            brojSlogova = Komanda.ExecuteNonQuery();
            Veza.Close();
            Veza.Dispose();

            return (brojSlogova > 0);
        }

    }
}
