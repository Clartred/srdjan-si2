﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KlasePodataka
{
    public class VrstaUslugeLista
    {

        private List<VrstaUsluge> listaVrstaUsluga;

        public List<VrstaUsluge> ListaVrstaUsluga
        {
            get
            {
                return listaVrstaUsluga;
            }
            set
            {
                if (this.listaVrstaUsluga != value)
                    this.listaVrstaUsluga = value;
            }
        }

        public VrstaUslugeLista()
        {
            listaVrstaUsluga = new List<VrstaUsluge>();
        }

        public void DodajElementListe(VrstaUsluge vrstaUsluge)
        {
            listaVrstaUsluga.Add(vrstaUsluge);
        }

        public void ObrisiElementListe(VrstaUsluge vrstaUsluge)
        {
            listaVrstaUsluga.Remove(vrstaUsluge);
        }

        public void ObrisiElementNaPoziciji(int pozicija)
        {
            listaVrstaUsluga.RemoveAt(pozicija);
        }

        public void IzmeniElementListe(VrstaUsluge staraVrstaUsluge, VrstaUsluge novaVrstaUsluge)
        {
            int idStareVrsteUsluge = 0;
            idStareVrsteUsluge = listaVrstaUsluga.IndexOf(staraVrstaUsluge);
            listaVrstaUsluga.RemoveAt(idStareVrsteUsluge);
            listaVrstaUsluga.Insert(idStareVrsteUsluge, novaVrstaUsluge);
        }

    }
}
