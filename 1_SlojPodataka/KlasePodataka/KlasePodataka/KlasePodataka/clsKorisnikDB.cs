﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using System.Data;
using System.Data.SqlClient;

namespace KlasePodataka
{
    public class clsKorisnikDB
    {

        // atributi
        private string pStringKonekcije;

        // property
        // 1. nacin
        public string StringKonekcije
        {
            get
            {
                return pStringKonekcije;
            }
            set // OVO NIJE DOBRO, MOZE SE STRING KONEKCIJE STAVITI NA PRAZAN STRING
            {
                if (this.pStringKonekcije != value)
                    this.pStringKonekcije = value;
            }
        }
        // konstruktor
        // 2. nacin prijema vrednosti stringa konekcije spolja i dodele atributu
        public clsKorisnikDB(string NoviStringKonekcije)
        // OVO JE DOBRO JER OBAVEZUJE DA SE PRILIKOM INSTANCIRANJA OVE KLASE
        // MORA OBEZBEDITI STRING KONEKCIJE
        {
            pStringKonekcije = NoviStringKonekcije;
        }

        // privatne metode

        // javne metode
        public DataSet DajKorisnikaPoKorisnickomImenuISifri(string korisnikoIme, string sifra)
        {
            DataSet dsPodaci = new DataSet();

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();
            SqlCommand Komanda = new SqlCommand("DajKorisnikaPoKorisnickomImenuISifri", Veza);
            Komanda.Parameters.Add("@KorisnickoIme", SqlDbType.NVarChar).Value = korisnikoIme;
            Komanda.Parameters.Add("@Sifra", SqlDbType.NVarChar).Value = sifra;
            Komanda.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = Komanda;
            da.Fill(dsPodaci);
            Veza.Close();
            Veza.Dispose();

            return dsPodaci;
        }

        public bool SnimiNovogKorisnika(string ime, string prezime, string korisnickoIme, string sifra)
        {

            if (DaLiJeKorisnickoImeZauzeto(korisnickoIme))
            {
                throw new Exception("Korisnicko ime je zauzeto");
            }
            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();

            SqlCommand Komanda = new SqlCommand("SnimiNovogKorisnika", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@Ime", SqlDbType.NVarChar).Value = ime;
            Komanda.Parameters.Add("@Prezime", SqlDbType.NVarChar).Value = prezime;
            Komanda.Parameters.Add("@KorisnickoIme", SqlDbType.NVarChar).Value = korisnickoIme;
            Komanda.Parameters.Add("@Sifra", SqlDbType.NVarChar).Value = sifra;

            int brojSlogova = Komanda.ExecuteNonQuery();
            Veza.Close();
            Veza.Dispose();


            return (brojSlogova > 0);
        }

        public bool DaLiJeKorisnickoImeZauzeto(string korisnickoIme)
        {

            DataSet dsPodaci = new DataSet();

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();
            SqlCommand Komanda = new SqlCommand("DaLiJeKorisnickoImeZauzeto", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@KorisnickoIme", SqlDbType.NVarChar).Value = korisnickoIme;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = Komanda;
            da.Fill(dsPodaci);
            Veza.Close();
            Veza.Dispose();

            return (dsPodaci.Tables[0].Rows[0].ItemArray[0].ToString() != null);
        }
    }

}
