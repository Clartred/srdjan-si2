﻿//
using KlaseMapiranja;
using KlasePodataka;
using System.Data;

namespace PoslovnaLogika
{
    public class clsPoslovnaPravila
    {
        // atributi
        private string pStringKonekcije;

        // property

        // konstruktor
        public clsPoslovnaPravila(string NoviStringKonekcije)
        {
            pStringKonekcije = NoviStringKonekcije;
        }

        // public metode
        public bool DaLiKorisnikMozeDaNaruciUslugu(int idKorisnika)
        {
            int brojNeplacenihUsluga = 0;
            UslugaDB uslugaDB = new UslugaDB(pStringKonekcije);
            DataSet listaUsluga = uslugaDB.DajSveUslugePoIdKorisnika(idKorisnika);
            foreach (DataTable table in listaUsluga.Tables)
            {
                foreach (DataRow row in table.Rows)
                {
                    foreach (DataColumn column in table.Columns)
                    {
                        if (row["Placena"].ToString() == "false") {
                            brojNeplacenihUsluga++;
                        }
                    }
                }
            }
            return true;
        }
    }
}
