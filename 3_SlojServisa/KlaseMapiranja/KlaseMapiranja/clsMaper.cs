﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//ukljucivanje klase podataka
using KlasePodataka;

namespace KlaseMapiranja
{
    public class clsMaper
    {
       
        private string pStringKonekcije;

        
        public clsMaper(string NoviStringKonekcije)
        {
            pStringKonekcije = NoviStringKonekcije;
        }


        public int DajIdUslugeZaWebServis(int IdUslugeIzBazePodataka)
        {
            UslugaDB vrstaUsluga = new UslugaDB(pStringKonekcije);
            int id = Int32.Parse(vrstaUsluga.DajUsluguPoId(IdUslugeIzBazePodataka).ToString());
            return id;           
        }

    }
}
