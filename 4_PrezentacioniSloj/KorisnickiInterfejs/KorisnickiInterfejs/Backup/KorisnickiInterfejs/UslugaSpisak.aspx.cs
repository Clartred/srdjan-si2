﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//
using System.Data;
using System.Configuration;
using PrezentacionaLogika;

namespace KorisnickiInterfejs
{
    public partial class UslugaSpisak : System.Web.UI.Page
    {
        // atributi
        //prezentaciona logika
        private clsFormaUslugaSpisak objFormaUslugaSpisak;

        // nase metode
        private void NapuniGrid(DataSet ds)
        {
            gvUsluga.DataSource = ds.Tables[0];
            gvUsluga.DataBind();
            // povezivanje grida sa datasetom
        }
         
        // dogadjaji
        protected void Page_Load(object sender, EventArgs e)
        {
            objFormaUslugaSpisak = new clsFormaUslugaSpisak(ConfigurationManager.ConnectionStrings["NasaKonekcija"].ConnectionString);  
        }

        protected void btnFiltriraj_Click(object sender, EventArgs e)
        {
            NapuniGrid(objFormaUslugaSpisak.DajPodatkeZaGrid(txbFilter.Text));   
        }

        protected void btnSvi_Click(object sender, EventArgs e)
        {
            NapuniGrid(objFormaUslugaSpisak.DajPodatkeZaGrid(""));   
        }
    }
}