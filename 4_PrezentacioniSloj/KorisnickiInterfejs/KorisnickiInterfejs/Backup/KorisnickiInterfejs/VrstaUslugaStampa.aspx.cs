﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//
using System.Data;
using PrezentacionaLogika;
using System.Configuration; 

namespace KorisnickiInterfejs
{
    public partial class VrstaUslugaStampa : System.Web.UI.Page
    {
        // atributi

        // nase metode
        private void NapuniGrid(DataSet ds)
        {
            // povezivanje grida sa datasetom
            gvSpisakUsluga.DataSource = ds.Tables[0];
            gvSpisakUsluga.DataBind();
         
        }
        
                
        protected void Page_Load(object sender, EventArgs e)
        {
            clsFormaVrstaUslugaStampa objFormaVrstaUslugaStampa = new clsFormaVrstaUslugaStampa(ConfigurationManager.ConnectionStrings["NasaKonekcija"].ConnectionString);
            string filter = Request.QueryString["filter"].ToString();

            if (filter.Equals(""))
            {
                lblNaslov.Text = "SPISAK SVIH VRSTA USLUGA"; 
            }
            else
            {
                lblNaslov.Text = "FILTRIRANI SPISAK VRSTA USLUGA, naziv=" + filter; 
            }

            NapuniGrid(objFormaVrstaUslugaStampa.DajPodatkeZaGrid(filter)); 
        }
    }
}