﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//
using System.Data;
using PrezentacionaLogika;
using System.Configuration; 

namespace KorisnickiInterfejs
{
    public partial class VrstaUslugeDetaljiEdit : System.Web.UI.Page
    {
        // atributi
        private int pId = 0;
        clsFormaVrstaUslugeDetaljiEdit objFormaVrstaUslugeDetaljiEdit; 
        
        // nase metode
        private void IsprazniKontrole()
        {
            txbId.Text = "";
            txbNaziv.Text = "";
            txbPocetnaCena.Text = "";

        }

        private void AktivirajKontrole()
        {
            txbId.Enabled = true;
            txbNaziv.Enabled = true;
            txbPocetnaCena.Enabled = true;
        }

        private void DeaktivirajKontrole()
        {
            txbId.Enabled = false;
            txbNaziv.Enabled = false;
            txbPocetnaCena.Enabled = false;
        }

        private void PrikaziPodatke(clsFormaVrstaUslugeDetaljiEdit objFormaVrstaUslugaDetaljiEdit)
        {
            // podacima stranice upravlja klasa prezentacione logike, zato se uzimaju iz nje za prikaz
            txbId.Text = Convert.ToString(objFormaVrstaUslugaDetaljiEdit.IdPreuzeteVrsteUsluga);
            txbNaziv.Text = objFormaVrstaUslugaDetaljiEdit.NazivPreuzeteVrsteUsluga;
            txbPocetnaCena.Text = Convert.ToString(objFormaVrstaUslugaDetaljiEdit.PocetnaCenaPreuzeteVrsteUsluga);
        }

        // dogadjaji
        protected void Page_Load(object sender, EventArgs e)
        {
            objFormaVrstaUslugeDetaljiEdit = new clsFormaVrstaUslugeDetaljiEdit(ConfigurationManager.ConnectionStrings["NasaKonekcija"].ConnectionString);
            pId = int.Parse(Request.QueryString["Id"].ToString());
            objFormaVrstaUslugeDetaljiEdit.IdPreuzeteVrsteUsluga = pId;
            // OVDE SE NE DOBIJA NAZIV SPOLJA, VEC SE IZRACUNAVA NAZIV na set svojstvu property za sifru UNUTAR KLASE  
            if (!IsPostBack)
            {
                PrikaziPodatke(objFormaVrstaUslugeDetaljiEdit);
            }  
        }

        protected void btnObrisi_Click(object sender, EventArgs e)
        {
            objFormaVrstaUslugeDetaljiEdit.IdPreuzeteVrsteUsluga = int.Parse(txbId.Text);
            bool uspehBrisanja = objFormaVrstaUslugeDetaljiEdit.ObrisiVrstuUsluge();
            if (uspehBrisanja)
            {
                lblStatus.Text = "Uspesno obrisan zapis!";
                IsprazniKontrole();
            }
            else
            {
                lblStatus.Text = "NEUSPEH BRISANJA zapisa!";
            }
        }

        protected void btnIzmeni_Click(object sender, EventArgs e)
        {
            // PREUZIMANJE POCETNIH, STARIH VREDNOSTI - OVDE NEMA POTREBE JER SE URADI NA PAGE LOAD
            //objFormaTimDetaljiEdit.IdTimaPreuzetogTima = Convert.ToInt32(txbId.Text);
            // - ovo se izracuna iz sifre, pa se ne moze ni dodeliti: objFormaZvanjeDetaljiEdit.NazivPreuzetogZvanja = txbNaziv.Text; 
            AktivirajKontrole();
            txbId.Focus();
 
        }

        protected void btnSnimiIzmenu_Click(object sender, EventArgs e)
        {
            objFormaVrstaUslugeDetaljiEdit.IdIzmenjeneVrstaUsluge = int.Parse(txbId.Text);
            objFormaVrstaUslugeDetaljiEdit.NazivIzmenjeneVrsteUsluga = txbNaziv.Text;
            objFormaVrstaUslugeDetaljiEdit.PocetnaCenaIzmenjeneVrsteUsluga = int.Parse(txbPocetnaCena.Text);
            bool uspehIzmene = objFormaVrstaUslugeDetaljiEdit.IzmeniVrstuUsluge();
            if (uspehIzmene)
            {
                lblStatus.Text = "Uspesno izmenjen zapis!";
                IsprazniKontrole();
            }
            else
            {
                lblStatus.Text = "NEUSPEH BRISANJA zapisa!";
            }
            DeaktivirajKontrole();
        }

       
    }
}