﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//
using System.Data;
using PrezentacionaLogika;
using System.Configuration; 

namespace KorisnickiInterfejs
{
    public partial class VrstaUslugeTabelaEdit : System.Web.UI.Page
    {
        
        // nase metode
        private void NapuniGrid(DataSet ds)
        {
            // povezivanje grida sa datasetom
            gvSpisakVrstaUslugaEdit.DataSource = ds.Tables[0];

            gvSpisakVrstaUslugaEdit.DataBind();
        }
        
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                clsFormaVrstaUslugeTabelaEdit objFormaVrstaUslugeTabelaEdit = new clsFormaVrstaUslugeTabelaEdit(ConfigurationManager.ConnectionStrings["NasaKonekcija"].ConnectionString);
                NapuniGrid(objFormaVrstaUslugeTabelaEdit.DajPodatkeZaGrid(""));
            }
        }



        protected void gvSpisakTimovaEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Redirect("VrstaUslugeDetaljiEdit.aspx?Sifra=" + gvSpisakVrstaUslugaEdit.Rows[gvSpisakVrstaUslugaEdit.SelectedIndex].Cells[1].Text);
        }

        
       
    }
}