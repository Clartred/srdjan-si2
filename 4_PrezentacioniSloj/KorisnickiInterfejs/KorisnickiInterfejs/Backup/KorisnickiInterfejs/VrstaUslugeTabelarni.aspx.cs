﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//
using DBUtils;
using System.Configuration;
using System.Data;

namespace KorisnickiInterfejs
{
    public partial class VrstaUslugeTabelarni : System.Web.UI.Page
    {
              
        
        private void NapuniGrid(DataSet ds)
        {
            // povezivanje grida sa datasetom
            gvSpisakVrstaUsluga.DataSource = ds.Tables[0];
            gvSpisakVrstaUsluga.DataBind();
        }
        
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnFiltriraj_Click(object sender, EventArgs e)
        {
            KlasePodataka.VrstaUslugeDB objVrstaUslugeDB = new KlasePodataka.VrstaUslugeDB(ConfigurationManager.ConnectionStrings["NasaKonekcija"].ToString());
            NapuniGrid(objVrstaUslugeDB.DajVrstUslugePoNazivu(txbFilter.Text)); 
        }

        protected void btnSvi_Click(object sender, EventArgs e)
        {
            KlasePodataka.VrstaUslugeDB clsVrstaUslugeDB = new KlasePodataka.VrstaUslugeDB(ConfigurationManager.ConnectionStrings["NasaKonekcija"].ToString());
            NapuniGrid(clsVrstaUslugeDB.DajSveVrsteUsluga()); 
        }

        
    }
}