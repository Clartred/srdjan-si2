﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="NarucivanjeUsluge.aspx.cs" Inherits="KorisnickiInterfejs.NarucivanjeUsluge" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
    {
        width: 423px;
            margin-left: 40px;
        }
    .style2
    {
        width: 342px;
        text-align: right;
    }
    .style3
    {
        width: 423px;
        font-size: large;
    }
    .style4
    {
        font-size: large;
    }
    .style5
    {
        width: 342px;
        font-size: large;
        text-align: right;
    }
        .style6
        {
            width: 342px;
            text-align: right;
            height: 26px;
        }
        .style7
        {
            width: 423px;
            margin-left: 40px;
            height: 26px;
        }
        .style8
        {
            height: 26px;
        }
        .auto-style1 {
            width: 342px;
            text-align: right;
            height: 29px;
        }
        .auto-style2 {
            width: 423px;
            margin-left: 40px;
            height: 29px;
        }
        .auto-style3 {
            height: 29px;
        }
        .auto-style4 {
            width: 342px;
            text-align: right;
            height: 25px;
        }
        .auto-style5 {
            width: 423px;
            margin-left: 40px;
            height: 25px;
        }
        .auto-style6 {
            height: 25px;
        }
        .auto-style7 {
            width: 100%;
            height: 1042px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table class="auto-style7">
    <tr>
        <td class="style5">
            &nbsp;</td>
        <td class="style3">
            <strong>NARUCIVANJE USLUGE</strong></td>
        <td class="style4">
            &nbsp;</td>
    </tr>
    <tr>
        <td class="style2">
            <asp:Label ID="Label2" runat="server" Text="Naziv"></asp:Label>
        </td>
        <td class="style1">
            <asp:TextBox ID="txbNaziv" runat="server"></asp:TextBox>
        </td>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        <td class="style2">
            <asp:Label ID="Label3" runat="server" Text="Opis"></asp:Label>
        </td>
        <td class="style1">
            <asp:TextBox ID="txbOpis" runat="server" ontextchanged="txbIme_TextChanged" 
                Height="53px" Width="166px"></asp:TextBox>
        </td>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style4">
            <asp:Label ID="Label4" runat="server" Text="Rok"></asp:Label>
        </td>
        <td class="auto-style5">
            <asp:Calendar ID="KalendarRok" runat="server" BackColor="White" BorderColor="Black" BorderStyle="Solid" CellSpacing="1" Font-Names="Verdana" Font-Size="9pt" ForeColor="Black" Height="250px" NextPrevFormat="ShortMonth" OnSelectionChanged="Calendar1_SelectionChanged" Width="330px">
                <DayHeaderStyle Font-Bold="True" Font-Size="8pt" ForeColor="#333333" Height="8pt" />
                <DayStyle BackColor="#CCCCCC" />
                <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="White" />
                <OtherMonthDayStyle ForeColor="#999999" />
                <SelectedDayStyle BackColor="#333399" ForeColor="White" />
                <TitleStyle BackColor="#333399" BorderStyle="Solid" Font-Bold="True" Font-Size="12pt" ForeColor="White" Height="12pt" />
                <TodayDayStyle BackColor="#999999" ForeColor="White" />
            </asp:Calendar>
        </td>
        <td class="auto-style6">
            </td>
    </tr>
    <tr>
        <td class="style2">
            <asp:Label ID="Label5" runat="server" Text="Cena"></asp:Label>
        </td>
        <td class="style1">
            <asp:TextBox ID="txbCena" runat="server"></asp:TextBox>
        </td>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        <td class="style2">
            <asp:Label ID="Label6" runat="server" Text="Pocetak izrade"></asp:Label>
        </td>
        <td class="style1">
            <asp:Calendar ID="KalendarPocetakIzrade" runat="server" BackColor="White" BorderColor="Black" BorderStyle="Solid" CellSpacing="1" Font-Names="Verdana" Font-Size="9pt" ForeColor="Black" Height="250px" NextPrevFormat="ShortMonth" OnSelectionChanged="KalendarPocetakIzrade_SelectionChanged" Width="330px">
                <DayHeaderStyle Font-Bold="True" Font-Size="8pt" ForeColor="#333333" Height="8pt" />
                <DayStyle BackColor="#CCCCCC" />
                <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="White" />
                <OtherMonthDayStyle ForeColor="#999999" />
                <SelectedDayStyle BackColor="#333399" ForeColor="White" />
                <TitleStyle BackColor="#333399" BorderStyle="Solid" Font-Bold="True" Font-Size="12pt" ForeColor="White" Height="12pt" />
                <TodayDayStyle BackColor="#999999" ForeColor="White" />
            </asp:Calendar>
        </td>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style1">
            <asp:Label ID="Label7" runat="server" Text="Kraj izrade"></asp:Label>
        </td>
        <td class="auto-style2">
            <asp:Calendar ID="KalendarKrajIzrade" runat="server" BackColor="White" BorderColor="Black" BorderStyle="Solid" CellSpacing="1" Font-Names="Verdana" Font-Size="9pt" ForeColor="Black" Height="250px" NextPrevFormat="ShortMonth" Width="330px">
                <DayHeaderStyle Font-Bold="True" Font-Size="8pt" ForeColor="#333333" Height="8pt" />
                <DayStyle BackColor="#CCCCCC" />
                <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="White" />
                <OtherMonthDayStyle ForeColor="#999999" />
                <SelectedDayStyle BackColor="#333399" ForeColor="White" />
                <TitleStyle BackColor="#333399" BorderStyle="Solid" Font-Bold="True" Font-Size="12pt" ForeColor="White" Height="12pt" />
                <TodayDayStyle BackColor="#999999" ForeColor="White" />
            </asp:Calendar>
        </td>
        <td class="auto-style3">
            </td>
    </tr>
    <tr>
        <td class="style6">
            <asp:Label ID="Label8" runat="server" Text="Placena"></asp:Label>
        </td>
        <td class="style7">
            <asp:CheckBox ID="ckDa" runat="server" Text="Da" />
            <asp:CheckBox ID="ckNe" runat="server" Text="Ne" />
        </td>
        <td class="style8">
            </td>
    </tr>
    <tr>
        <td class="style2">
            <asp:Label ID="lblTim" runat="server" Text="Vrsta usluge"></asp:Label>
        </td>
        <td class="style1">
            <asp:DropDownList ID="dllVrstaUsluge" runat="server" Height="16px" Width="233px" 
                onselectedindexchanged="ddlZvanje_SelectedIndexChanged" 
                style="margin-bottom: 0px">
            </asp:DropDownList>
        </td>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style4">
        </td>
        <td class="auto-style5">
            <asp:Label ID="lblStatus" runat="server" Text="STATUS"></asp:Label>
        </td>
        <td class="auto-style6">
            </td>
    </tr>
    <tr>
        <td class="style2">
            &nbsp;</td>
        <td class="style1">
            <asp:Button ID="btnSnimi" runat="server" Text="SNIMI" Width="69px" 
                onclick="btnSnimi_Click" />
            <asp:Button ID="btnPonisti" runat="server" Text="PONISTI" 
                onclick="btnPonisti_Click" />
        </td>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        <td class="style2">
            &nbsp;</td>
        <td class="style1">
            &nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        <td class="style2">
            &nbsp;</td>
        <td class="style1">
            &nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style4">
            </td>
        <td class="auto-style5">
        </td>
        <td class="auto-style6">
            </td>
    </tr>
    <tr>
        <td class="style2">
            &nbsp;</td>
        <td class="style1">
            &nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
</table>
</asp:Content>
