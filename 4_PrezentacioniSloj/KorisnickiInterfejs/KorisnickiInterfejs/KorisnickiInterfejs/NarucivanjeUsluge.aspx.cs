﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//
using System.Data;
using System.Configuration;
using PrezentacionaLogika;

namespace KorisnickiInterfejs
{
    public partial class NarucivanjeUsluge : System.Web.UI.Page
    {
        // atributi
        //prezentaciona logika
        clsFormaUslugeUnos objFormaUslugaUnos;
     
        private void NapuniCombo()
        {
            // IZDVAJANJE PODATAKA IZ XML POSREDSTVOM WEB SERVISA
            DataSet ds = new DataSet();
            ds = objFormaUslugaUnos.DajPodatkeZaCombo();

            int ukupno = ds.Tables[0].Rows.Count;

            // PUNJENJE COMBO PODACIMA IZ DATASETA
            dllVrstaUsluge.Items.Add("Izaberite...");
         
            for (int i = 0; i < ukupno; i++)
            {
                dllVrstaUsluge.Items.Add(ds.Tables[0].Rows[i].ItemArray[1].ToString());
            }

        }

        private void IsprazniKontrole()
        {
            KalendarRok.SelectedDate = DateTime.Today; 
            KalendarPocetakIzrade.SelectedDate = DateTime.Today;
            KalendarKrajIzrade.SelectedDate = DateTime.Today;
            txbNaziv.Text = "";
            txbOpis.Text = "";      
            txbCena.Text = "";
            ckDa.Text = "";
            ckNe.Text = "";
            dllVrstaUsluge.Text = "Izaberite...";
            lblStatus.Text = ""; 
        }


        // dogadjaji
        protected void Page_Load(object sender, EventArgs e)
        {
            objFormaUslugaUnos = new clsFormaUslugeUnos(ConfigurationManager.ConnectionStrings["NasaKonekcija"].ConnectionString);
            if (!IsPostBack)
            {
                NapuniCombo();
            }
        }

        protected void btnSnimi_Click(object sender, EventArgs e) 
        {
            // ***********preuzimanje vrednosti sa korisnickog interfejsa
            // 2. nacin - preuzimaju atributi klase prezentacione logike
            objFormaUslugaUnos.Naziv = txbNaziv.Text;
            objFormaUslugaUnos.Opis = txbOpis.Text;
            objFormaUslugaUnos.Rok = Convert.ToDateTime(KalendarRok.SelectedDate.ToString());
            objFormaUslugaUnos.Cena = int.Parse(txbCena.Text);
            objFormaUslugaUnos.PocetakIzrade = Convert.ToDateTime(KalendarPocetakIzrade.SelectedDate.ToString());
            objFormaUslugaUnos.Placena = Convert.ToBoolean(ckDa.Text);
            objFormaUslugaUnos.Placena = Convert.ToBoolean(ckNe.Text);
            objFormaUslugaUnos.KrajIzrade = Convert.ToDateTime(KalendarKrajIzrade.SelectedDate.ToString());
            objFormaUslugaUnos.Naziv = dllVrstaUsluge.Text;  
            
            // provera ispravnosti vrednosti
            // provera popunjenosti
            bool SvePopunjeno = objFormaUslugaUnos.DaLiJeSvePopunjeno();

            //provera ispravnosti - karakteri, vrednost iz domena, jedinstvenost zapisa
            bool JedinstvenZapis = objFormaUslugaUnos.DaLiJeJedinstvenZapis();


            //provera ispravnosti - provera uskladjenosti podataka sa poslovnim pravilima
            bool UskladjenoSaPoslovnimPravilima = objFormaUslugaUnos.DaLiSuPodaciUskladjeniSaPoslovnimPravilima();

            //snimanje u bazu podataka
            string porukaStatusaSnimanja = "";
            if (SvePopunjeno)
            {
                if (JedinstvenZapis)
                {
                    if (UskladjenoSaPoslovnimPravilima)
                    {
                        // snimanje podataka
                        objFormaUslugaUnos.SnimiPodatke();
                        // priprema teksta poruke o uspehu snimanja
                        porukaStatusaSnimanja = "USPESNO SNIMLJENI PODACI!";
                    }
                    else
                    {
                        porukaStatusaSnimanja = "PODACI NISU U SKLADU SA POSLOVNIM PRAVILIMA!";
                    }
                }
                else
                {
                    porukaStatusaSnimanja = "VEC POSTOJI USLUGA SA ISTIM ID-OM!";
                }
            }
            else
            { 
                // priprema teksta poruke o gresci
                porukaStatusaSnimanja = "NISU SVI PODACI POPUNJENI!";
             //   txbId.Focus();  
            }


            //obavestavanje korisnika o statusu snimanja
            lblStatus.Text = porukaStatusaSnimanja; 


        }

        protected void btnPonisti_Click(object sender, EventArgs e)
        {
            IsprazniKontrole();
        }

        protected void ddlZvanje_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void txbIme_TextChanged(object sender, EventArgs e)
        {

        }

        protected void KalendarPocetakIzrade_SelectionChanged(object sender, EventArgs e)
        {

        }
    }
}