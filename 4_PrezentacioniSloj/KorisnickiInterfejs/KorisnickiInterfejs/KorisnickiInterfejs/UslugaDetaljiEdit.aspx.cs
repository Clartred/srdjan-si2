﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//
using System.Data;
using PrezentacionaLogika;
using System.Configuration; 

namespace KorisnickiInterfejs
{
    public partial class UslugaDetaljiEdit : System.Web.UI.Page
    {
        // atributi
        private int pId = 0;
        clsFormaUslugaDetaljiEdit objFormaUslugaDetaljiEdit; 
        
        // nase metode
        private void IsprazniKontrole()
        {
            txbNaziv.Text = "";
            txbOpis.Text = "";
            KalendarRok.SelectedDate = DateTime.Today;
            KalendarPocetakIzrade.SelectedDate = DateTime.Today;
            KalendarKrajIzrade.SelectedDate = DateTime.Today;
            txbCena.Text = "";
            cbDa.Text = "";
            cbNe.Text = "";
        }

        private void AktivirajKontrole()
        {
            txbNaziv.Enabled = true;
            txbOpis.Enabled = true;
            KalendarRok.Enabled = true;
            KalendarPocetakIzrade.Enabled = true;
            KalendarKrajIzrade.Enabled = true;
            txbCena.Enabled = true;
            cbDa.Enabled = true;
            cbNe.Enabled = true;
        }

        private void DeaktivirajKontrole()
        {
            txbNaziv.Enabled = false;
            txbOpis.Enabled = false;
            KalendarRok.Enabled = false;
            KalendarPocetakIzrade.Enabled = false;
            KalendarKrajIzrade.Enabled = false;
            txbCena.Enabled = false;
            cbDa.Enabled = false;
            cbNe.Enabled = false;
        }

        private void PrikaziPodatke(clsFormaUslugaDetaljiEdit objFormaUslugaDetaljiEdit)
        {
            txbOpis.Text = objFormaUslugaDetaljiEdit.NazivPreuzeteUsluge;
           
        }

        // dogadjaji
        protected void Page_Load(object sender, EventArgs e)
        {
            objFormaUslugaDetaljiEdit = new clsFormaUslugaDetaljiEdit(ConfigurationManager.ConnectionStrings["NasaKonekcija"].ConnectionString);
            pId = int.Parse(Request.QueryString["Id"].ToString());
            objFormaUslugaDetaljiEdit.IdPreuzeteUsluge = pId;
            // OVDE SE NE DOBIJA NAZIV SPOLJA, VEC SE IZRACUNAVA NAZIV na set svojstvu property za sifru UNUTAR KLASE  
            if (!IsPostBack)
            {
                PrikaziPodatke(objFormaUslugaDetaljiEdit);
            }  
        }

        protected void btnObrisi_Click(object sender, EventArgs e)
        {
            objFormaUslugaDetaljiEdit.IdPreuzeteUsluge = int.Parse(txbNaziv.Text);
            bool uspehBrisanja = objFormaUslugaDetaljiEdit.ObrisiUslugu();
            if (uspehBrisanja)
            {
                lblStatus.Text = "Uspesno obrisan zapis!";
                IsprazniKontrole();
            }
            else
            {
                lblStatus.Text = "NEUSPEH BRISANJA zapisa!";
            }
        }

        protected void btnIzmeni_Click(object sender, EventArgs e)
        {
            AktivirajKontrole();
            txbNaziv.Focus();
 
        }

        protected void btnSnimiIzmenu_Click(object sender, EventArgs e)
        {
            objFormaUslugaDetaljiEdit.IdIzmenjeneUsluge = int.Parse(txbNaziv.Text);
            objFormaUslugaDetaljiEdit.NazivIzmenjeneUsluge = txbOpis.Text;
            objFormaUslugaDetaljiEdit.OpisIzmenjeneUsluge = txbOpis.Text;
            objFormaUslugaDetaljiEdit.RokIzmenjeneUsluge = Convert.ToDateTime(KalendarRok.SelectedDate.ToString());
            objFormaUslugaDetaljiEdit.PocetakIzradeIzmenjeneUsluge = Convert.ToDateTime(KalendarPocetakIzrade.SelectedDate.ToString());
            objFormaUslugaDetaljiEdit.KrajIzradeIzmenjeneUsluge = Convert.ToDateTime(KalendarKrajIzrade.SelectedDate.ToString());
            objFormaUslugaDetaljiEdit.CenaIzmenjeneUsluge = int.Parse(txbCena.Text);
            objFormaUslugaDetaljiEdit.PlacenaIzmenjeneUsluge = Convert.ToBoolean(cbDa.Text);
            objFormaUslugaDetaljiEdit.PlacenaIzmenjeneUsluge = Convert.ToBoolean(cbNe.Text);
            bool uspehIzmene = objFormaUslugaDetaljiEdit.IzmeniUslugu();
            if (uspehIzmene)
            {
                lblStatus.Text = "Uspesno izmenjen zapis!";
                IsprazniKontrole();
            }
            else
            {
                lblStatus.Text = "NEUSPEH BRISANJA zapisa!";
            }
            DeaktivirajKontrole();
        }

       
    }
}