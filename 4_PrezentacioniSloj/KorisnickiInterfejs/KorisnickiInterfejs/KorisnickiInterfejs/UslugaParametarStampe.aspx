﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="UslugaParametarStampe.aspx.cs" Inherits="KorisnickiInterfejs.UslugaParametarStampe" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            text-align: right;
            width: 385px;
        }
        .style2
        {
            text-align: right;
            width: 385px;
            height: 30px;
        }
        .style3
        {
            height: 30px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table style="width:100%;">
        <tr>
            <td class="style1">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style2">
                <asp:Label ID="Label1" runat="server" style="text-align: right" 
                    Text="Vrsta usluge"></asp:Label>
            </td>
            <td class="style3">
                <asp:TextBox ID="txbFilter" runat="server" 
                    style="margin-bottom: 0px; margin-top: 1px;"></asp:TextBox>
                <asp:Button ID="btnFilterStampa" runat="server" onclick="btnFilterStampa_Click" 
                    Text="PRIKAZI FILTRIRANI SPISAK ZA STAMPU" Width="297px" />
            </td>
            <td class="style3">
                </td>
        </tr>
        <tr>
            <td class="style1">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>
