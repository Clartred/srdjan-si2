﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//
using System.Data;
using System.Configuration;
using PrezentacionaLogika;

namespace KorisnickiInterfejs
{
    public partial class UslugaSpisak : System.Web.UI.Page
    {
        //prezentaciona logika
        private clsFormaUslugaSpisak objFormaUslugaSpisak;

        private void NapuniCombo()
        {
            // IZDVAJANJE PODATAKA IZ XML POSREDSTVOM WEB SERVISA
            DataSet ds = new DataSet();
            ds = objFormaUslugaSpisak.DajPodatkeZaCombo();

            int ukupno = ds.Tables[0].Rows.Count;

            // PUNJENJE COMBO PODACIMA IZ DATASETA
            dllVrstaUsluge.Items.Add("Izaberite...");

            for (int i = 0; i < ukupno; i++)
            {
                dllVrstaUsluge.Items.Add(ds.Tables[0].Rows[i].ItemArray[1].ToString());
            }

        }
        private void NapuniGrid(DataSet ds)
        {
            gvUsluga.DataSource = ds.Tables[0];
            gvUsluga.DataBind();
         
        }
     
        protected void Page_Load(object sender, EventArgs e)
        {
            objFormaUslugaSpisak = new clsFormaUslugaSpisak(ConfigurationManager.ConnectionStrings["NasaKonekcija"].ConnectionString);
            if (!IsPostBack)
            {
                NapuniCombo();
            }
        }

        protected void btnFiltriraj_Click(object sender, EventArgs e)
        {
            NapuniGrid(objFormaUslugaSpisak.DajPodatkeZaGrid(dllVrstaUsluge.SelectedIndex));   
        }

        protected void btnSvi_Click(object sender, EventArgs e)
        {
            NapuniGrid(objFormaUslugaSpisak.DajPodatkeZaGrid(dllVrstaUsluge.SelectedIndex));   
        }
    }
}