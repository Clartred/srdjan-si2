﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//
using System.Data;
using PrezentacionaLogika;
using System.Configuration; 

namespace KorisnickiInterfejs
{
    public partial class UslugaStampa : System.Web.UI.Page
    {
        private void NapuniGrid(DataSet ds)
        {
            // povezivanje grida sa datasetom
            gvSpisakUsluga.DataSource = ds.Tables[0];
            gvSpisakUsluga.DataBind();
         
        }
                
        protected void Page_Load(object sender, EventArgs e)
        {
            clsFormaUslugaStampa objFormaUslugaStampa = new clsFormaUslugaStampa(ConfigurationManager.ConnectionStrings["NasaKonekcija"].ConnectionString);
            int filter = Convert.ToInt32(Request.QueryString["filter"].ToString());

            if (filter.Equals(""))
            {
                lblNaslov.Text = "SPISAK SVIH USLUGA"; 
            }
            else
            {
                lblNaslov.Text = "FILTRIRANI SPISAK USLUGA PO VRSTI USLUGE, naziv=" + filter; 
            }

            NapuniGrid(objFormaUslugaStampa.DajPodatkeZaGrid(filter)); 
        }
    }
}