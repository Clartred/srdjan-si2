﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//
using System.Data;
using PrezentacionaLogika;
using System.Configuration; 

namespace KorisnickiInterfejs
{
    public partial class UslugaTabelaEdit : System.Web.UI.Page
    {
        private string pStringKonekcije;
        public UslugaTabelaEdit(string NoviStringKonekcije)
        {
            pStringKonekcije = NoviStringKonekcije;
        }
        private void NapuniGrid(DataSet ds)
        {
            // povezivanje grida sa datasetom
            gvSpisakUslugaEdit.DataSource = ds.Tables[0];

            gvSpisakUslugaEdit.DataBind();
        }

        public DataSet DajSveUsluge()
        {
            clsFormaUslugaTabelaEdit uslugaTabelaEdit = new clsFormaUslugaTabelaEdit(pStringKonekcije);
            return uslugaTabelaEdit.DajSveUsluge();
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                clsFormaUslugaTabelaEdit objFormaUslugaTabelaEdit = new clsFormaUslugaTabelaEdit(ConfigurationManager.ConnectionStrings["NasaKonekcija"].ConnectionString);
                NapuniGrid(objFormaUslugaTabelaEdit.DajSveUsluge());
            }
        }



        protected void gvSpisakUslugaEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Redirect("UslugaDetaljiEdit.aspx?Sifra=" + gvSpisakUslugaEdit.Rows[gvSpisakUslugaEdit.SelectedIndex].Cells[1].Text);
        }

        
       
    }
}