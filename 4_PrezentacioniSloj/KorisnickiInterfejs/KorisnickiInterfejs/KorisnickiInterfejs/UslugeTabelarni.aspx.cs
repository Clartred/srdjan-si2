﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//
using DBUtils;
using KlasePodataka;
using PrezentacionaLogika;
using System.Configuration;
using System.Data;

namespace KorisnickiInterfejs
{
    public partial class UslugeTabelarni : System.Web.UI.Page
    {
        clsFormaUslugaSpisak objFormaUslugaSpisak;

        private void NapuniCombo()
        {
            DataSet ds = new DataSet();
            ds = objFormaUslugaSpisak.DajPodatkeZaCombo();

            int ukupno = ds.Tables[0].Rows.Count;

            // PUNJENJE COMBO PODACIMA IZ DATASETA
            dllVrstaUsluge.Items.Add("Izaberite...");

            for (int i = 0; i < ukupno; i++)
            {
                dllVrstaUsluge.Items.Add(ds.Tables[0].Rows[i].ItemArray[1].ToString());
            }

        }


        private void NapuniGrid(DataSet ds)
        {
            // povezivanje grida sa datasetom
            gvSpisakUsluga.DataSource = ds.Tables[0];
            gvSpisakUsluga.DataBind();
        }



        protected void Page_Load(object sender, EventArgs e)
        {

            objFormaUslugaSpisak = new clsFormaUslugaSpisak(ConfigurationManager.ConnectionStrings["NasaKonekcija"].ConnectionString);
            if (!IsPostBack)
            {
                NapuniCombo();  
            }
        }

        protected void btnFiltriraj_Click(object sender, EventArgs e)
        {
            KlasePodataka.UslugaDB objUslugaDB = new KlasePodataka.UslugaDB(ConfigurationManager.ConnectionStrings["NasaKonekcija"].ToString());
            NapuniGrid(objUslugaDB.DajUslugePoVrstiUsluge(dllVrstaUsluge.SelectedIndex)); 

        }

        protected void btnSvi_Click(object sender, EventArgs e)
        {
            KlasePodataka.VrstaUslugeDB objVrstaUslugeDB = new KlasePodataka.VrstaUslugeDB(ConfigurationManager.ConnectionStrings["NasaKonekcija"].ToString());
            NapuniGrid(objVrstaUslugeDB.DajSveVrsteUsluga()); 
        }

        
    }
}