﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//
using DBUtils;
using System.Configuration; 

namespace KorisnickiInterfejs
{
    public partial class VrstaUslugeUnos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSnimi_Click(object sender, EventArgs e)
        {
            KlasePodataka.VrstaUslugeDB objVrstaUslugeDB = new KlasePodataka.VrstaUslugeDB(ConfigurationManager.ConnectionStrings["NasaKonekcija"].ToString());
            KlasePodataka.VrstaUsluge objVrstaUsluge = new KlasePodataka.VrstaUsluge();
            objVrstaUsluge.Naziv = txbNaziv.Text;
            objVrstaUsluge.PocetnaCena = int.Parse(txbPocetnaCena.Text);
            objVrstaUslugeDB.SnimiNovuVrstuUsluge(objVrstaUsluge);
            lblStatus.Text = "Snimljeno";
        }
    }
}