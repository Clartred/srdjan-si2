﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using System.Data;
using KlasePodataka;

namespace PrezentacionaLogika
{
    public class clsFormaUslugaDetaljiEdit
    {
   // atributi 
        private string pStringKonekcije;
        private UslugaDB objUslugaDB;

        private Usluga objPreuzetaUsluga;
        private Usluga objIzmenjenaUsluga;

        private int pIdPreuzeteUsluge;
        private string pNazivPreuzeteUsluge;
        private string pOpisPreuzeteUsluge;
        private DateTime pRokPreuzeteUsluge;
        private int pCenaPreuzeteUsluge;
        private DateTime pPocetakIzradePreuzeteUsluge;
        private DateTime pKrajIzradePreuzeteUsluge;
        private bool pPlacenaPreuzetaUsluga;

        private int pIdIzmenjeneUsluge;
        private string pNazivIzmenjeneUsluge;
        private string pOpisIzmenjeneUsluge;
        private DateTime pRokIzmenjeneUsluge;
        private int pCenaIzmenjeneUsluge;
        private DateTime pPocetakIzradeIzmenjeneUsluge;
        private DateTime pKrajIzradeIzmenjeneUsluge;
        private bool pPlacenaIzmenjeneUsluge;

        public int IdPreuzeteUsluge
        {
            get { return pIdPreuzeteUsluge; }
            set { pIdPreuzeteUsluge = value;
          //  pNazivPreuzeteUsluge = DajUslugePoVrstiUsluge(pIdPreuzeteUsluge);
            }
        }

        public string NazivPreuzeteUsluge
        {
            get { return pNazivPreuzeteUsluge; }

        }

        public string OpisPreuzeteUsluge
        {
            get { return pOpisPreuzeteUsluge; }
            
        }

        public DateTime RokPreuzeteUsluge
        {
            get { return pRokPreuzeteUsluge; }
        }

        public int CenaPreuzeteUsluge
        {
            get { return pCenaPreuzeteUsluge; }

        }
        public DateTime PocetakIzradePreuzeteUsluge
        {
            get { return pKrajIzradeIzmenjeneUsluge; }
        }
            
        public DateTime KrajIzradePreuzeteUsluge
        {
            get { return pKrajIzradePreuzeteUsluge; }
        }

        public bool PlacenaPreuzetaUsluge
        {
            get { return pPlacenaPreuzetaUsluga; }
            
        }

        public int IdIzmenjeneUsluge
        {
            get { return pIdIzmenjeneUsluge; }
            set { pIdIzmenjeneUsluge = value; }
        }

        public string NazivIzmenjeneUsluge
        {
            get { return pNazivIzmenjeneUsluge; }
            set { pNazivIzmenjeneUsluge = value; }
        }

        public string OpisIzmenjeneUsluge
        {
            get { return pOpisIzmenjeneUsluge; }
            set { pOpisIzmenjeneUsluge = value; }
        }


        public DateTime RokIzmenjeneUsluge
        {
            get { return pRokIzmenjeneUsluge; }
            set { pRokIzmenjeneUsluge = value; }
        }


        public int CenaIzmenjeneUsluge
        {
            get { return pCenaIzmenjeneUsluge; }
            set { pCenaIzmenjeneUsluge = value; }
        }


        public DateTime PocetakIzradeIzmenjeneUsluge
        {
            get { return pPocetakIzradeIzmenjeneUsluge; }
            set { pPocetakIzradeIzmenjeneUsluge = value; }
        }

        public DateTime KrajIzradeIzmenjeneUsluge
        {
            get { return pKrajIzradeIzmenjeneUsluge; }
            set { pKrajIzradeIzmenjeneUsluge = value; }
        }

        public bool PlacenaIzmenjeneUsluge
        {
            get { return pPlacenaIzmenjeneUsluge; }
            set { pPlacenaIzmenjeneUsluge = value; }
        }

    // konstruktor
        public clsFormaUslugaDetaljiEdit(string NoviStringKonekcije)
        {
            pStringKonekcije = NoviStringKonekcije;
            objUslugaDB = new UslugaDB(pStringKonekcije);
        }

        // privatne metode
        private DataSet DajUslugePoVrstiUsluge(int IdUsluge)
        {
            return objUslugaDB.DajUslugePoVrstiUsluge(IdUsluge); 
        }

        // javne metode
        public bool ObrisiUslugu()
        {
            
            bool uspehBrisanja = false;
            uspehBrisanja = objUslugaDB.ObrisiUslugu(pIdPreuzeteUsluge);

            return uspehBrisanja;

        }

        public bool IzmeniUslugu()
        {
            bool uspehIzmene = false;
            objPreuzetaUsluga = new Usluga();
            objIzmenjenaUsluga = new Usluga();

            objPreuzetaUsluga.Id = pIdPreuzeteUsluge;
            objPreuzetaUsluga.Naziv = pNazivPreuzeteUsluge;
            objPreuzetaUsluga.Opis = pOpisPreuzeteUsluge;
            objPreuzetaUsluga.Rok = pRokPreuzeteUsluge;
            objPreuzetaUsluga.Cena = pCenaPreuzeteUsluge;
            objPreuzetaUsluga.PocetakIzrade = pPocetakIzradePreuzeteUsluge;
            objPreuzetaUsluga.KrajIzrade = pKrajIzradePreuzeteUsluge;

            objIzmenjenaUsluga.Id = pIdIzmenjeneUsluge;
            objIzmenjenaUsluga.Naziv = pNazivIzmenjeneUsluge;
            objIzmenjenaUsluga.Opis = pOpisIzmenjeneUsluge;
            objIzmenjenaUsluga.Rok = pRokIzmenjeneUsluge;
            objIzmenjenaUsluga.Cena = pCenaIzmenjeneUsluge;
            objIzmenjenaUsluga.PocetakIzrade = pPocetakIzradeIzmenjeneUsluge;
            objIzmenjenaUsluga.KrajIzrade = pKrajIzradeIzmenjeneUsluge;


            uspehIzmene = objUslugaDB.IzmeniUslugu(objPreuzetaUsluga, objIzmenjenaUsluga);
            return uspehIzmene;
        }
    }
}
