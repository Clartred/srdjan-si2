﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using System.Data;
using KlasePodataka;

namespace PrezentacionaLogika
{
    public class clsFormaUslugaSpisak
    {
        // atributi
        private string pStringKonekcije;

        // konstruktor
        public clsFormaUslugaSpisak(string NoviStringKonekcije)
        {
            pStringKonekcije = NoviStringKonekcije;
        }

        public DataSet DajPodatkeZaCombo()
        {
            DataSet dsPodaci = new DataSet();
            UslugaDB objUslugaDB = new UslugaDB(pStringKonekcije);

            dsPodaci = objUslugaDB.DajSveUsluge();

            return dsPodaci;
        }

        // public metode
        public DataSet DajPodatkeZaGrid(int filter)
        {
            DataSet dsPodaci = new DataSet();
            UslugaDB objUslugaDB = new UslugaDB(pStringKonekcije);
            if (filter.Equals(""))
            {
                dsPodaci = objUslugaDB.DajSveUsluge(); 
            }
            else
            {
                dsPodaci = objUslugaDB.DajUslugePoVrstiUsluge(filter);
            }
            return dsPodaci;
        }

    }
}
