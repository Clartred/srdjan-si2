﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using System.Data;
using KlasePodataka;

namespace PrezentacionaLogika
{
    public class clsFormaUslugaStampa
    {
        // atributi
        private string pStringKonekcije;

        // konstruktor
        public clsFormaUslugaStampa(string NoviStringKonekcije)
        {
            pStringKonekcije = NoviStringKonekcije;
        }

        // public metode
        public DataSet DajPodatkeZaGrid(int filter)
        {
            DataSet dsPodaci = new DataSet();
            UslugaDB objUslugaDB = new UslugaDB(pStringKonekcije);            
            if (filter.Equals(""))
            {
                dsPodaci = objUslugaDB.DajSveUsluge(); 
            }
            else
            {   
                dsPodaci = objUslugaDB.DajUslugePoVrstiUsluge(filter); 
            }
            return dsPodaci;
        }
    }
}
