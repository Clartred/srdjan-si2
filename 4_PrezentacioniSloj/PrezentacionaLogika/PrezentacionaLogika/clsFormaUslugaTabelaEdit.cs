﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using System.Data;
using KlasePodataka;

namespace PrezentacionaLogika
{
    public class clsFormaUslugaTabelaEdit
    {
        // atributi
        private string pStringKonekcije;
        
        // konstruktor
        public clsFormaUslugaTabelaEdit(string NoviStringKonekcije)
        {
            pStringKonekcije = NoviStringKonekcije;
        }

        public DataSet DajSveUsluge() 
        {
            UslugaDB uslugaDB = new UslugaDB(pStringKonekcije);
            return uslugaDB.DajSveUsluge();
        }

        // public metode
        public DataSet DajPodatkeZaGrid(int filter)
        {
            DataSet dsPodaci = new DataSet();
            UslugaDB objUslugaDB = new UslugaDB(pStringKonekcije);            
            if (filter.Equals(""))
            {
                dsPodaci = objUslugaDB.DajSveUsluge(); 
            }
            else
            {
                dsPodaci = objUslugaDB.DajUslugePoVrstiUsluge(filter); 
            }
            return dsPodaci;
        }
    }
}
