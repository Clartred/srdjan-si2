﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using System.Data;
using KlasePodataka;
using PoslovnaLogika;

namespace PrezentacionaLogika
{
    public class clsFormaUslugeUnos
    {
        // atributi
        private string pStringKonekcije;
        private int pId;
        private int pIdKorisnika;
        private string pNaziv;
        private DateTime pRok;
        private int pCena;
        private string pOpis;
        private DateTime pPocetakIzrade;
        private DateTime pKrajIzrade;
        private bool pPlacena = false;
        private VrstaUsluge vrstaUsluge;

        public int Id
        {
            get { return pId; }
            set { pId = value; }
        }

        public int IdKorisnika
        {
            get { return pIdKorisnika; }
            set { pIdKorisnika = value; }
        }

        public string Naziv
        {
            get { return pNaziv; }
            set { pNaziv = value; }
        }

        public int Cena
        {
            get { return pCena; }
            set { pCena = value; }
        }
        public string Opis
        {
            get { return pOpis; }
            set { pOpis = value; }
        }
        public DateTime Rok
        {
            get { return pRok; }
            set { pRok = value; }
        }
        public DateTime PocetakIzrade
        {
            get { return pPocetakIzrade; }
            set { pPocetakIzrade = value; }
        }

        public DateTime KrajIzrade
        {
            get { return pKrajIzrade; }
            set { pKrajIzrade = value; }
        }

        public bool Placena
        {
            get { return pPlacena; }
            set { pPlacena = value; }
        }
        public VrstaUsluge VrstaUsluge
        {
            get { return vrstaUsluge; }
            set { vrstaUsluge = value; }
        }
        
        // konstruktor
        public clsFormaUslugeUnos(string NoviStringKonekcije)
        {
            pStringKonekcije = NoviStringKonekcije;
        }

        // private metode

        // public metode
        public DataSet DajPodatkeZaCombo()
        {
            DataSet dsPodaci = new DataSet();
            VrstaUslugeDB objVrstaUslugeDB = new VrstaUslugeDB(pStringKonekcije);

            dsPodaci = objVrstaUslugeDB.DajSveVrsteUsluga();
            
            return dsPodaci;
        }

        public bool DaLiJeSvePopunjeno()
        {
            bool SvePopunjeno = false;

      
            // PRERADA KODA - IF (da li nesto jeste)

            if ((pId > 0) && (pIdKorisnika > 0) && (pNaziv.Length > 0) && (pRok != null) && (pCena > 0) && (pOpis.Length > 0) && (pPocetakIzrade != null) && (pKrajIzrade != null) && (pPlacena != null) &&(!pNaziv.Equals("Izaberite...")))
            {
                SvePopunjeno = true;
            }
            else
            {
                SvePopunjeno = false;
            }

            return SvePopunjeno;
        }


        public bool DaLiJeJedinstvenZapis()
        {
            bool JedinstvenZapis = false;
            DataSet dsPodaci = new DataSet();
            UslugaDB objUslugeDB = new UslugaDB(pStringKonekcije);
            dsPodaci = objUslugeDB.DajUsluguPoId(pId);
            
            if (dsPodaci.Tables[0].Rows.Count == 0)
            {
                JedinstvenZapis = true;
            }
            else
            {
                JedinstvenZapis = false;
            }

            return JedinstvenZapis;

        }

        public bool SnimiPodatke()
        {
            bool uspehSnimanja=false;

            UslugaDB objUslugeDB = new UslugaDB(pStringKonekcije);
            Usluga objNovaUsluga = new Usluga();
            objNovaUsluga.Id = pId;
            objNovaUsluga.Naziv = pNaziv;
            objNovaUsluga.Rok = pRok;
            objNovaUsluga.Cena = pCena;
            objNovaUsluga.Opis = pOpis;
            objNovaUsluga.PocetakIzrade = pPocetakIzrade;
            objNovaUsluga.KrajIzrade = pKrajIzrade;
            objNovaUsluga.Placena = pPlacena;

            VrstaUsluge objVrstaUsluge = new VrstaUsluge();

            VrstaUslugeDB objVrstaUslugeDB = new VrstaUslugeDB(pStringKonekcije);
            objVrstaUsluge.Id = Convert.ToInt32(objVrstaUslugeDB.DajIdUslugePoNazivu(pNaziv));
            objVrstaUsluge.Naziv = pNaziv;

            objNovaUsluga.VrstaUsluge = objVrstaUsluge;

            uspehSnimanja = objUslugeDB.SnimiNovuUslugu(objNovaUsluga);


            return uspehSnimanja;
        }

        public bool DaLiSuPodaciUskladjeniSaPoslovnimPravilima()
        {
            bool UskladjeniPodaci = false;

            clsPoslovnaPravila objPoslovnaPravila = new clsPoslovnaPravila(pStringKonekcije);

            VrstaUslugeDB objVrstaUslugeDB = new VrstaUslugeDB(pStringKonekcije);
            int Id = Convert.ToInt32(objVrstaUslugeDB.DajIdUslugePoNazivu(pNaziv));


            UskladjeniPodaci = objPoslovnaPravila.DaLiKorisnikMozeDaNaruciUslugu(pIdKorisnika);

            return UskladjeniPodaci;
        }
    }
}
